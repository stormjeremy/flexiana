(ns spa.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [reloaded.repl :refer [system init start stop go reset]]
            [spa.systems :as s])
  (:gen-class))

(defn -main [& args]
  (reloaded.repl/set-init! s/system)
  (go))
