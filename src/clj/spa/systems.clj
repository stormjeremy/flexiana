(ns spa.systems
  (:require
   [clojure.core.async :as async]
   [com.stuartsierra.component :as component]
   [environ.core :refer [env]]
   [io.pedestal.http  :as http]
   [spa.api.core :as api]
   [spa.components.pedestal :refer [new-pedestal]]))

(def service-map
  {::http/routes         api/routes
   ::http/type           :jetty
   ::http/port           8890
   ::http/secure-headers {:content-security-policy-settings {:object-src "none"}}
   ::http/resource-path  "./"
   ::http/file-path      "./"
   ::http/join?          false})

(defn system []
  (-> (component/system-map
       :pedestal  (new-pedestal service-map))
      (component/system-using
       {:pedestal []})))
