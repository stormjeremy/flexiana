(ns spa.components.pedestal
  (:require
   [com.stuartsierra.component :as component]
   [clojure.tools.logging :as log]
   [io.pedestal.http :as http]
   [io.pedestal.http.route :as route]
   [io.pedestal.interceptor :refer [interceptor interceptor-name]]
   [spa.protocols :as ps]))

(defrecord Pedestal [config server]
  component/Lifecycle
  (start [{:keys [config server] :as component}]
    (log/info "Starting Pedestal")
    (if server
      component
      (assoc component
             :server (http/start (http/create-server config)))))
  (stop [{:keys [server] :as component}]
    (log/info "Stopping Pedestal")
    (try
      (http/stop server)
      (catch Exception e (format "Caught exception: %s" (.getMessage e))))
    (assoc component :server nil))

  ps/Pedestal
  (service-fn [{:keys [server]}]
    (::http/service-fn server)))
(defn new-pedestal [config]
  (map->Pedestal {:config config}))
