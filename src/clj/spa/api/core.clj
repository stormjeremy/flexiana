(ns spa.api.core
  (:require
   [clojure.tools.logging :as log]
   [io.pedestal.http :as server]
   [io.pedestal.http.route :as route]))

;;; Like (apply merge-with - [coll1 coll2]),
;;; except it fails if a key exist in the second map and not the first
(defn merge-minus [colls]
  (apply
   reduce
   (fn [acc [k v]]
     (if (acc k)
       (update acc k #(- % v))
       (reduced {:failure-to-merge -1})))
   colls))

(defn scramble [& words]
  (->> words
       (map frequencies)
       merge-minus
       vals
       (every? (complement neg?))))

(def flexiana
  {:name :flexiana
   :enter
   (fn [context]
     (let [{:keys [word1 word2]} (get-in context [:request :path-params])]
       (assoc context
              :response {:status 200 :body (format "Scrambled %s" (scramble word1 word2))})))})

(def routes
  (route/expand-routes
   #{["/api/flex/:word1/:word2" :get flexiana :route-name :flex]}))
