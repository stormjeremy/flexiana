(ns spa.core
  (:require
   [cljs-http.client :as http]
   [cljs.core.async :refer [<!]]
   [reagent.core :as r])
  (:require-macros
   [cljs.core.async.macros :refer [go]])
  (:import [goog.string format]))

(enable-console-print!)

(def app (r/atom {}))

(defn scramble-callback [state w1 w2]
  (go
    (swap! state assoc :result (:body (<! (http/get (format "/api/flex/%s/%s" w1 w2)))))))

(defn spa [state]
  (let [{:keys [result word1 word2]} @app]
    [:div
     [:h1 "Flexiana problems:"]
     (when result
       [:h2 (format "Flexiana solution: %s" result)])
     [:input {:value word1 :on-change #(swap! state assoc :word1 (-> % .-target .-value))}]
     [:input {:value word2 :on-change #(swap! state assoc :word2 (-> % .-target .-value))}]
     [:input {:type     :button
              :value    "submit"
              :on-click #(when (and word1 word2)
                           (scramble-callback state word1 word2 ))}]]))

(defn ^:export run []
  (r/render [spa app]
            (js/document.getElementById "app")))

(defonce runner (run))
