(set-env!
 :source-paths #{"src/clj/" "src/cljs/"}
 :resource-paths #{"resources/public"}
 :dependencies
 '[[adzerk/boot-cljs                   "2.1.5"                       ]
   [adzerk/boot-cljs-repl              "0.4.0"                       ]
   [adzerk/boot-reload                 "0.6.0"                       ]
   [adzerk/boot-test                   "1.2.0"          :scope "test"]
   [boot-environ                       "1.1.0"                       ]
   [ch.qos.logback/logback-classic     "1.1.3"                       ]
   [cider/piggieback                   "0.3.9"          :scope "test"]
   [clj-http                           "3.9.1"                       ]
   [cljs-http                          "0.1.45"                      ]
   [compojure                          "1.6.1"                       ]
   [com.stuartsierra/component         "0.3.2"                       ]
   [environ                            "1.1.0"                       ]
   [io.pedestal/pedestal.service       "0.5.4"                       ]
   [io.pedestal/pedestal.service-tools "0.5.4"                       ]
   [io.pedestal/pedestal.jetty         "0.5.4"                       ]
   [io.pedestal/pedestal.immutant      "0.5.4"                       ]
   [io.pedestal/pedestal.tomcat        "0.5.4"                       ]
   [io.pedestal/pedestal.aws           "0.5.4"                       ]
   [nrepl                              "0.4.5"          :scope "test"]
   [org.clojure/clojure                "1.9.0"                       ]
   [org.clojure/clojurescript          "1.10.439"                    ]
   [org.clojure/tools.logging          "0.4.1"                       ]
   [pandeiro/boot-http                 "0.8.3"                       ]
   [reagent                            "0.8.1"                       ]
   [reloaded.repl                      "0.2.4"                       ]
   [samestep/boot-refresh              "0.1.0"          :scope "test"]
   [weasel                             "0.7.0"          :scope "test"]])

(require
 '[adzerk.boot-cljs      :refer [cljs]]
 '[adzerk.boot-reload    :refer [reload]]
 '[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
 '[boot.core             :refer [deftask]]
 '[boot.task.built-in    :refer [install jar pom target watch]]
 '[environ.boot          :refer [environ]]
 '[pandeiro.boot-http    :refer [serve]]
 '[samestep.boot-refresh :refer [refresh]])

(def +version+ "0.1.1")

(deftask build
  "Build my project."
  []
  (comp (pom) (jar :file "project.jar" :main) (install)))

(deftask dev
  "Launch Dev Environment"
  []
  (merge-env! :source-paths   #{"dev/clj" "test/clj"})
  (merge-env! :resource-paths #{"dev/logging"})
  (comp
   (environ :env {:dev? "true"})
   (watch)
   (reload :on-jsload 'spa.core/run)
   (cljs-repl)
   (cljs)
   (target :dir #{"target"})))

(deftask uberjar
  "Create an uberjar"
  []
  (merge-env! :resource-paths #{"prod/logging"})
  (comp
   (cljs)
   (aot :all true)
   (uber)
   (jar :file "project.jar" :main 'spa.core)
   (sift :include #{#"project.jar"})
   (target)))
