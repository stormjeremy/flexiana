# Flexiana Test Project #

## Prerequisites
This project requires
* [boot](https://github.com/boot-clj/boot)


## Usage ##
To start development mode, simply run:

    boot dev

wait for the boot to finish fetching all of the required dependencies, and navigate
to localhost:8890/index.html. On the initial run of the application, you may need to reload the page.
