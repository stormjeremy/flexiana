(ns spa.components.pedestal-test
  (:require
   [clojure.tools.logging :as log]
   [com.stuartsierra.component :as component]
   [clojure.test :refer [are deftest is testing use-fixtures]]
   [io.pedestal.http  :as http]
   [io.pedestal.test :refer [response-for]]
   [reloaded.repl :refer [stop init start]]
   [spa.api.core :as api]
   [spa.components.pedestal :refer [new-pedestal]]
   [spa.protocols :as ps]))

(def service-map
  {::http/routes api/routes
   ::http/type   :jetty
   ::http/secure-headers {:content-security-policy-settings {:object-src "none"}}
   ::http/port   8891
   ::http/join?  false})

(defn initialize [f]
  (reloaded.repl/set-init!
   #(-> (component/system-map
         :pedestal (new-pedestal service-map))
        (component/system-using
         {:pedestal []})))
  (init)
  (start)
  (f)
  (stop))

(use-fixtures :once initialize)

(defn routes [{:keys [pedestal]}]
  (let [service-fn (ps/service-fn pedestal)]
    (testing "Pedestal Routes"
      (are [x y] (= x y)

        {:status 404, :body "Not Found", :headers {"Content-Type" "text/plain"}}
        (response-for service-fn :get "/a")

        {:status 404, :body "Not Found", :headers {"Content-Type" "text/plain"}}
        (response-for service-fn :get "/flexiana")

        "Scrambled true"
        (:body (response-for service-fn :get "/api/flex/rekqodlw/world"))

        "Scrambled false"
        (:body (response-for service-fn :get "/api/flex/rekqodlw/worldz"))))))

(deftest pedestal-test
  (routes reloaded.repl/system))
