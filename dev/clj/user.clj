(ns user
  (:require
   [environ.core      :refer [env]]
   [io.pedestal.test  :refer [response-for]]
   [io.pedestal.http  :as http]
   [reloaded.repl     :refer [init go reset reset-all start stop system]]
   [spa.systems       :as app]
   [spa.protocols     :as protocols]))


(defn initialize []
  (when (:dev? env)
    (stop)
    (reloaded.repl/set-init! app/system)
    (go)))

(defonce dev (initialize))
